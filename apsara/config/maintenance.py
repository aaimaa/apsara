from frappe import _

def get_data():
	return [
        {
			"label": _("Awesome Maintenance"),
			"icon": "icon-gear",
			"items": [
				{
					"type": "doctype",
					"name": "Equipment Maintenance Log",
					"description": _("Equipment Maintenance Log"),
				},
                {
					"type": "doctype",
					"name": "Computing Asset Inspection Checklist",
					"description": _("Computing Asset Inspection Checklist"),
				},
                {
					"type": "doctype",
					"name": "Fixed Asset Inspection Checklist",
					"description": _("Fixed Asset Inspection Checklist"),
				},
                {
					"type": "doctype",
					"name": "Generator Fuel Consumption Log",
					"description": _("Generator Fuel Consumption Log"),
				},
                {
					"type": "doctype",
					"name": "Daily Generator Activity Log",
					"description": _("Daily Generator Activity Log"),
				}
			]
		},
		{
			"label": _("Awesome Maintenance Reports"),
			"icon": "icon-paper-clip",
			"items": [
				{
					"type": "report",
					"is_query_report": True,
					"name": "Daily Generator Activity Log Report",
					"doctype": "Daily Generator Activity Log",
				},
				{
					"type": "report",
					"is_query_report": False,
					"name": "Generator Fuel Consumption Log Report",
					"doctype": "Generator Fuel Consumption Log",
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Fixed Asset Inspection Checklist Report",
					"doctype": "Fixed Asset Inspection Checklist",
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Computing Asset Inspection Checklist Report",
					"doctype": "Computing Asset Inspection Checklist",
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Equipment Maintenance Log Report",
					"doctype": "Equipment Maintenance Log",
				}
			]
		}
	]
